./activate.bat

.\prepare.ps1
.\install_application_packages.ps1
.\install_development_packages.ps1

Start-Process powershell 'git config --global user.email "peter.yordanov.dev@gmail.com"'
Start-Process powershell 'git config --global user.name "PeterYordanov"'

npm install -g @ionic/cli

Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
