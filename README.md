# Windows Reinstall Setup 3

[[_TOC_]]

## Run Script
```powershell
<#Open Powershell#>
Set-ExecutionPolicy Unrestricted
.\perform.ps1
```

## Preparation
- [x] Activate Windows 10
- [x] Unbloat Windows (Remove pre-installed apps)
  - [x] 4 Bing apps
  - [x] Get Started
  - [x] Mail
  - [x] Calendar
  - [x] One Note
  - [x] People
  - [x] Skype
  - [x] Xbox
  - [x] 2 Zune apps
  - [x] Solitaire
  - [x] Sticky Notes
  - [x] 3D Paint
  - [x] Get Help
  - [x] Alarms
  - [x] Twitter
  - [x] Candy Crush Saga
  - [x] Maps
  - [x] Camera
  - [x] Alarms
- [x] Disable Internet Explorer
- [x] Install Chocolatey

## Install Applications
- [x] NitroShare
- [x] qBittorrent
- [x] Adobe Reader
- [x] WinCDEMU
- [x] 7Zip
- [x] Notion
- [x] Everything
- [x] KeePass
- [x] MPV
- [x] Discord
- [x] Firefox
- [x] VirtualBox
- [x] curl
- [x] Rufus
- [x] RemNote
- [x] FreeDownloadManager
- [x] AOMEI Partition Assistant
   
## Install Development Apps
- [x] Qt Creator
- [x] Visual Studio
- [x] KDevelop
- [x] Robotic Operating System
- [x] Git
- [x] Git Fork
- [x] Velocity
- [x] DevBook
- [x] CMake
- [x] Docker
- [x] vcpkg
- [x] Rust
- [x] Notepad++
- [x] Visual Studio Code

## Final Steps
- [x] Configure Git
- [x] Enable Hyper-V
