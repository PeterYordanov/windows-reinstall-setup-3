Add-Type -AssemblyName PresentationCore,PresentationFramework

$ProvisionedAppPackageNames = @(
    "Microsoft.BingFinance"
    "Microsoft.BingNews"
    "Microsoft.BingSports"
    "Microsoft.BingWeather"
    "Microsoft.Getstarted"
    "microsoft.windowscommunicationsapps" # Mail,Calendar
    "Microsoft.Office.OneNote"
    "Microsoft.People"
    "Microsoft.SkypeApp"
    "Microsoft.XboxApp"
    "Microsoft.ZuneMusic"
    "Microsoft.ZuneVideo"
    "Microsoft.MicrosoftSolitaireCollection"
    "*Stickynotes*"
    "Microsoft.GetHelp"
    "*phone*"
    "*3d*"
    '*alarms*'
    "*bing*"
    "*camera*"
    "*candycrushsodasaga*"
    "*communi*"
    "*commsPhone*"
    "*connectivitystore*"
    "*maps*"
    "*sway*"
    "*twitter*"
)

foreach ($ProvisionedAppName in $ProvisionedAppPackageNames) {
    Write-Host "Uninstalling $ProvisionedAppName..."
    Get-AppxPackage -Name $ProvisionedAppName -AllUsers | Remove-AppxPackage
    Get-AppXProvisionedPackage -Online | Where-Object DisplayName -EQ $ProvisionedAppName | Remove-AppxProvisionedPackage -Online
}

Write-Host "Disabling Internet Explorer..."
Disable-WindowsOptionalFeature -online -FeatureName internet-explorer-optional-amd64

Write-Host "Installing Chocolatey..."
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
