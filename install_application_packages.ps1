$applications = @(  
                    'nitroshare',
                    'notion', 
                    'bleachbit',
                    'qbittorrent',
                    'simplenote',
                    'adobereader',
                    'wincdemu',
                    '7zip',
                    'everything',
                    'keepass',
                    'mpv',
                    'discord',
                    'firefox',
                    'curl',
                    'rufus',
                    'screentogif'
                )
$applications | ForEach-Object { choco install -y $_ }

curl -O kmymoney5-mingw64-5.0.6-8.1-setup.exe https://download.kde.org/stable/kmymoney/5.0.6/win64/kmymoney5-mingw64-5.0.6-8.1-setup.exe
$pathvargs = {.\kmymoney5-mingw64-5.0.6-8.1-setup.exe /S /v/qn }
Invoke-Command -ScriptBlock $pathvargs

curl -O PAssist_Std.exe https://www2.aomeisoftware.com/download/pa/PAssist_Std.exe
$pathvargs = {.\PAssist_Std.exe /S /v/qn }
Invoke-Command -ScriptBlock $pathvargs
