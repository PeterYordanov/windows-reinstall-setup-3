$dev_applications = @(  
                    'git',
                    'cmake',
                    'git-fork',
                    'docker-desktop',
                    'notepadplusplus',
                    'vscode',
                    'golang',
                    'nodejs'
                )
$dev_applications | ForEach-Object { choco install -y $_ }

curl -O DevBook.exe https://dl.todesktop.com/2102273jsy18baz/windows/nsis/x64
$pathvargs = {.\DevBook.exe /S /v/qn }
Invoke-Command -ScriptBlock $pathvargs

curl -O massCode.Setup.1.3.0.exe https://github.com/antonreshetov/massCode/releases/download/v1.3.0/massCode.Setup.1.3.0.exe
$pathvargs = {.\massCode.Setup.1.3.0.exe /S /v/qn }
Invoke-Command -ScriptBlock $pathvargs

curl -O rustup-init.exe https://static.rust-lang.org/rustup/dist/x86_64-pc-windows-msvc/rustup-init.exe
$pathvargs = {.\rustup-init.exe /S /v/qn }
Invoke-Command -ScriptBlock $pathvargs

dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
curl -O wsl_update_x64.msi https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi
$pathvargs = {.\wsl_update_x64.msi /S /v/qn }
Invoke-Command -ScriptBlock $pathvargs

wsl --set-default-version 2

#git clone https://github.com/microsoft/vcpkg.git
#https://visualstudio.microsoft.com/downloads/
#https://www.qt.io/download
##https://www.microsoft.com/bg-bg/p/windows-terminal/9n0dx20hk701?rtc=1&activetab=pivot:overviewtab
